@ECHO off
REM ###############################################################################
REM # @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
REM # 	All Rights Reserved.
REM # 	Unauthorized copying of this file, via any medium is strictly prohibited
REM # 	Dissemination of this information or reproduction of this material
REM # 	is strictly forbidden unless prior written permission is obtained
REM # 	from Guichet Entreprises.
REM ###############################################################################
CHCP 65001
MODE 100,40
SETLOCAL EnableDelayedExpansion
GOTO START
:PRINT_LINE <textVar>
(
    SET "LINE_HERE=%~1"
    @ECHO !LINE_HERE!
    exit /b
)
REM ###############################################################################
:START
CLS
CALL :PRINT_LINE "╔══════════════════════════════════════════════════════════════════════════════════════════════════╗"
CALL :PRINT_LINE "║                      ╔═╗┬ ┬┬┌─┐┬ ┬┌─┐┌┬┐  ╔═╗┌┐┌┌┬┐┬─┐┌─┐┌─┐┬─┐┬┌─┐┌─┐┌─┐                        ║"
CALL :PRINT_LINE "║                      ║ ╦│ │││  ├─┤├┤  │   ║╣ │││ │ ├┬┘├┤ ├─┘├┬┘│└─┐├┤ └─┐                        ║"
CALL :PRINT_LINE "║                      ╚═╝└─┘┴└─┘┴ ┴└─┘ ┴   ╚═╝┘└┘ ┴ ┴└─└─┘┴  ┴└─┴└─┘└─┘└─┘                        ║"
CALL :PRINT_LINE "╚══════════════════════════════════════════════════════════════════════════════════════════════════╝"
CALL :PRINT_LINE "╔══════════════════════════════════════════════════════════════════════════════════════════════════╗"
CALL :PRINT_LINE "║                                       _   __           __                                        ║"   
CALL :PRINT_LINE "║                                      / | / /___ ______/ /_                                       ║"   
CALL :PRINT_LINE "║                                     /  |/ / __ `/ ___/ __ \                                      ║"   
CALL :PRINT_LINE "║                                    / /|  / /_/ (__  ) / / /                                      ║"   
CALL :PRINT_LINE "║                                   /_/ |_/\__,_/____/_/ /_/                                       ║"   
CALL :PRINT_LINE "║                                                                                                  ║"   
CALL :PRINT_LINE "╚══════════════════════════════════════════════════════════════════════════════════════════════════╝"
@ECHO:
@ECHO:
SET FORMSTEMPLATE_PATH=%~dp0formstemplate.exe
@ECHO: FORMSTEMPLATE_PATH = %FORMSTEMPLATE_PATH%
@ECHO: CONF FILE = %1
@ECHO: OUTPUT = %~dp1
@ECHO:
CD /D %~dp0
REM -------------------------------------------------------------------------------
"%FORMSTEMPLATE_PATH%" --verbose --conf=%1 --overwrite=yes --output=%~dp1
REM -------------------------------------------------------------------------------
CHOICE /C:YN /M "Do it again ? (Y/N)"
IF "%ERRORLEVEL%" EQU "1" GOTO :START
IF "%ERRORLEVEL%" EQU "2" GOTO :END
REM -------------------------------------------------------------------------------
PAUSE
:END

#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
# 	All Rights Reserved.
# 	Unauthorized copying of this file, via any medium is strictly prohibited
# 	Dissemination of this information or reproduction of this material
# 	is strictly forbidden unless prior written permission is obtained
# 	from Guichet Entreprises.
###############################################################################

import logging
import sys
import os
import os.path

from pygenash import template

###############################################################################
#
###############################################################################
def all_profiler():
    folder = "C:\\dev\\users-guidance\\forms"
    yml_files = []
    for file in os.listdir(folder):
        if file.endswith(".yml"):
            yml_files.append(os.path.join(folder, file))

    for conf in yml_files:
        template.generate(conf, folder)

###############################################################################
#
###############################################################################
def test_template():
    assert template.std_folder()
    # template.generate(os.path.join(__get_this_folder(),
    #                                "test_template/start.yml"),
    #                   os.path.join(__get_this_folder(),
    #                                "test_template/")
    #                   )
    # template.generate(os.path.join(__get_this_folder(),
    #                                "test_profiler/profiler.yml"),
    #                   os.path.join(__get_this_folder(),
    #                                "test_profiler/")
    #                   )
    template.generate(os.path.join(__get_this_folder(),
                                   "faq/faq.yml"),
                      os.path.join(__get_this_folder(),
                                   "faq/")
                      )
    # template.generate(os.path.join(__get_this_folder(),
    #                                "test_template/create.yml"),
    #                   os.path.join(__get_this_folder(),
    #                                "test_template/test2")
    #                   )

###############################################################################
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the filename of THIS script.
###############################################################################
def __get_this_filename():
    result = ""

    if getattr(sys, 'frozen', False):
        # frozen
        result = sys.executable
    else:
        # unfrozen
        result = __file__

    return result

###############################################################################
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the folder of THIS script.
###############################################################################
def __get_this_folder():
    return os.path.split(os.path.abspath(os.path.realpath(
        __get_this_filename())))[0]


###############################################################################
# Set up the logging system
###############################################################################
def __set_logging_system():
    log_filename = os.path.splitext(os.path.abspath(
        os.path.realpath(__get_this_filename())))[0] + '.log'
    logging.basicConfig(filename=log_filename, level=logging.DEBUG,
                        format='%(asctime)s: %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p')
    console = logging.StreamHandler()
    console.setLevel(logging.DEBUG)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(asctime)s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

###############################################################################
# Main script call only if this script is runned directly
###############################################################################
def __main():
    # ------------------------------------
    logging.info('Started %s', __get_this_filename())
    logging.info('The Python version is %s.%s.%s',
                 sys.version_info[0], sys.version_info[1], sys.version_info[2])

    test_template()
    # all_profiler()

    logging.info('Finished')
    # ------------------------------------


###############################################################################
# Call main function if the script is main
# Exec only if this script is runned directly
###############################################################################
if __name__ == '__main__':
    __set_logging_system()
    __main()

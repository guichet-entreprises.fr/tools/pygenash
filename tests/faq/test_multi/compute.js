﻿// Save the target url
function memorize(target_url){
    nash.instance.load('target_url.xml').bind('target',{'url': target_url});
};

// Error function
function throwError(message){
    nash.instance.load('display.xml').bind('page',{'error': message});
    memorize("/record/" + nash.record.description().getRecordUid() + "/0/page/0");
};

// All targets
var destination = "";
var selection = _INPUT_.page

// ----------------------------------------------------------------------------


if (Value('id').of(selection.dataId).eq("Idx01")) {
	if (Value('id').of(selection.dataIdx01).eq("Idx01x01")) {
		destination = "ok";
	}
	if (Value('id').of(selection.dataIdx01).eq("Idx01x02")) {
		if (Value('id').of(selection.dataIdx01x02).eq("Idx01x02x01")) {
			destination = "Tu t'appelle BERNARD";
		}
		if (Value('id').of(selection.dataIdx01x02).eq("Idx01x02x02")) {
			destination = "T'es un inconnu";
		}
	}
}
if (Value('id').of(selection.dataId).eq("Idx02")) {
	if (Value('id').of(selection.dataIdx02).eq("Idx02x01")) {
		destination = "ok";
	}
	if (Value('id').of(selection.dataIdx02).eq("Idx02x02")) {
		if (Value('id').of(selection.dataIdx02x02).eq("Idx02x02x01")) {
			destination = "Tu t'appelle BERNARD";
		}
		if (Value('id').of(selection.dataIdx02x02).eq("Idx02x02x02")) {
			destination = "T'es un inconnu";
		}
	}
}
if (Value('id').of(selection.dataId).eq("Idx03")) {
	if (Value('id').of(selection.dataIdx03).eq("Idx03x01")) {
		destination = "ref id de la formalité profiler ou autre";
	}
	if (Value('id').of(selection.dataIdx03).eq("Idx03x02")) {
		if (Value('id').of(selection.dataIdx03x02).eq("Idx03x02x01")) {
			destination = "Tu t'appelle BERNARD";
		}
		if (Value('id').of(selection.dataIdx03x02).eq("Idx03x02x02")) {
			destination = "T'es un inconnu";
		}
	}
	if (Value('id').of(selection.dataIdx03).eq("Idx03x03")) {
		destination = "Formalité truc";
	}
}
// ----------------------------------------------------------------------------

var the_destination = {
    'target':destination
};

if (destination.includes(":")) {
    var explode = destination.split(":",2);
    the_destination['nash_target'] = explode[0];
    the_destination['target'] = explode[1];
}


var the_destination = destination[choice];
var target_url = "/"
if ('nash_target' in the_destination) {
    conf_target_url = _CONFIG_.get(the_destination['nash_target']+'.public.url');	
	if(conf_target_url == null){
        log.error('formUid = {} | nash_target = {} | nash_target doesn\'t exist', nash.record.description().formUid, the_destination['nash_target']);
        return throwError('La formalité cible est introuvable. La paramètre "nash_target" est incorrect.'); 
	}
    target_url = conf_target_url + "/";
}

if ('target' in the_destination) {
    target_url = target_url + "form/use?reference=" + encodeURIComponent(the_destination['target']);
} else {
    return throwError('La formalité cible est introuvable.');
}

log.info('formUid = {} | choice = {} | target url = {}', nash.record.description().formUid, choice, target_url);

// redirect
return memorize(target_url);
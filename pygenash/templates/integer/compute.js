// Error function
function memorize(target_url){
    nash.instance.load('target_url.xml').bind('target',{'url': target_url});
};

function between(x, min, max) {
    return (x >= min || min == "*") && (x <= max || max == "*");
}

// Error function
function throwError(message){
    var error_display = nash.instance.load('_error_display.xml');
    error_display.bind('page.text',{'description': message})
    
    // var error_display = nash.instance.load('display.xml');
    // error_display.bind('choice',{'error': message})
    
    nash.record.stepsMgr().requestDone(nash.record.currentStepPosition());
    var target_url = "/record/" + nash.record.description().getRecordUid() + "/1/page/0";
    log.info('formUid = {} | target url = {} | message={}', nash.record.description().formUid, target_url, message);
    memorize(target_url);
} 

// All targets
var destination = {};

{%- for item in display.choice %}
destination['{{item.id}}'] = {};

{%- if 'target' in item %}
destination['{{item.id}}']['target'] = {{item.target|tojson}};
{%- endif -%}

{%- if 'nash_target' in item %}
destination['{{item.id}}']['nash_target'] = {{item.nash_target|tojson}};
{%- endif -%}

{%- if 'range' in item %}
destination['{{item.id}}']['range'] = {{item.range|tojson}};
{%- endif -%}

{%- if 'error_text' in item %}
destination['{{item.id}}']['error_text'] = {{item.error_text|tojson}};
{%- else %}
destination['{{item.id}}']['error_text'] = "La destination n'est pas définie.";
{%- endif -%}

{%- endfor %}

// test the length of the destination
if (destination.length == 0){
    return memorize("/");
}

if (_INPUT_.choice.selected == null) {
    var recordUid = nash.record.description().getRecordUid();
    log.info("No integer filled -> Redirection to the first page for the record {}", recordUid);
    return memorize("/record/" + recordUid + "/0/page/0");
}

// user choice from display
var integer = _INPUT_.choice.selected;

var target_url = "/"
for(var key in destination){
    //tokenizer my_int
    var my_range = destination[key]['range'].split("-");
    log.error("range : " + my_range + " integer : " + integer);
                
    if(between(integer, my_range[0], my_range[1])){
        var the_destination = destination[key];
        if ('nash_target' in the_destination) {
            conf_target_url = _CONFIG_.get(the_destination['nash_target']+'.public.url');	
            if(conf_target_url == null){
                log.error('formUid = {} | nash_target = {} | nash_target doesn\'t exist', nash.record.description().formUid, the_destination['nash_target']);
                return throwError('La formalité cible est introuvable. La paramètre "nash_target" est incorrect.'); 
            }
            target_url = conf_target_url + "/";
        }
        
        if ('target' in the_destination) {
            target_url = target_url + "form/use?reference=" + encodeURIComponent(the_destination['target']);
        } else {
            return throwError(the_destination['error_text']);
        }
    }
}

if (target_url == "/" ) { 
    log.error('formUid = {} | integer = {} | choice not in target', nash.record.description().formUid, integer);
    return memorize("/record/" + recordUid + "/0/page/0"); 
    //return throwError("formUid = {} | integer = " + integer +" | choice not in target" )
}



log.info('formUid = {} | choice = {} | target url = {}', nash.record.description().formUid, integer, target_url);

// redirect
return memorize(target_url);